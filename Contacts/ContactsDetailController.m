//
//  ContactsDetailController.m
//  Contacts
//
//  Created by Lyndy Tankersley on 2/23/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "ContactsDetailController.h"

@interface ContactsDetailController ()

@end

@implementation ContactsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    
    firstNameFld = [UITextField new];
    firstNameFld.borderStyle = UITextBorderStyleRoundedRect;
    firstNameFld.placeholder = @"First name";
    firstNameFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:firstNameFld];
    
    lastNameFld = [UITextField new];
    lastNameFld.borderStyle = UITextBorderStyleRoundedRect;
    lastNameFld.placeholder = @"Last name";
    lastNameFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:lastNameFld];
    
    phoneFld = [UITextField new];
    phoneFld.borderStyle = UITextBorderStyleRoundedRect;
    phoneFld.placeholder = @"Phone number";
    phoneFld.translatesAutoresizingMaskIntoConstraints = NO;
    
    addressFld = [UITextField new];
    addressFld.borderStyle = UITextBorderStyleRoundedRect;
    addressFld.placeholder = @"Address";
    addressFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:addressFld];
    
    emailFld = [UITextField new];
    emailFld.borderStyle = UITextBorderStyleRoundedRect;
    emailFld.placeholder = @"Email field";
    emailFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:emailFld];
    
    [self.view addSubview:phoneFld];
    
    UIButton *callBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    callBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [callBtn setTitle:@"Call" forState:UIControlStateNormal];
    [callBtn addTarget:self action:@selector(callBtnTouched:) forControlEvents:UIControlEventTouchDown];
    [callBtn sizeToFit];
    
    UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    mapBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [mapBtn setTitle:@"Map" forState:UIControlStateNormal];
    [mapBtn addTarget:self action:@selector(mapBtnTouched:) forControlEvents:UIControlEventTouchDown];
    [mapBtn sizeToFit];
    
    [self.view addSubview:callBtn];
    [self.view addSubview:mapBtn];
    
    
    
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(firstNameFld,lastNameFld, phoneFld, addressFld, emailFld, callBtn, mapBtn);
    
     NSDictionary *metrics = @{@"margin":[NSNumber numberWithInt:20], @"topmargin":[NSNumber numberWithInt:70]};
    

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[firstNameFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[lastNameFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[phoneFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[addressFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[emailFld]-margin-|" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[callBtn]" options:0 metrics:metrics views:viewDict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[mapBtn]" options:0 metrics:metrics views:viewDict]];
    
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[firstNameFld]-10-[lastNameFld]-10-[phoneFld]-10-[addressFld]-10-[emailFld]-10-[callBtn]-10-[mapBtn]" options:0 metrics:metrics views:viewDict]];
    
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnTouched:)];
    
    UIBarButtonItem* sendBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendBtnTouched:)];
    
    
    self.navigationItem.rightBarButtonItems = @[saveBtn,sendBtn];
    
    if(self.contact){
        firstNameFld.text = [self.contact valueForKey:@"firstname"];
        addressFld.text = [self.contact valueForKey:@"address"];
        lastNameFld.text = [self.contact valueForKey:@"lastname"];
        phoneFld.text = [self.contact valueForKey:@"phone"];
        emailFld.text = [self.contact valueForKey:@"email"];
    }
}

-(void)callBtnTouched:(id)sender{
    NSLog(@"call button pressed");
    
    NSString* phoneNum = [[NSString alloc]initWithFormat:@"tel:%@",phoneFld.text];
    NSURL* phoneNumURL = [NSURL URLWithString:phoneNum];
    [[UIApplication sharedApplication]openURL:phoneNumURL options:@{} completionHandler:nil];
}

-(void)mapBtnTouched:(id)sender{
    NSLog(@"Map button pressed");
    NSString* address = [[NSString alloc]initWithFormat:@"http://maps.apple.com/?q=%@",addressFld.text];
    NSURL* addressURL = [NSURL URLWithString:address];
    [[UIApplication sharedApplication]openURL:addressURL options:@{} completionHandler:nil];

}

-(void)saveBtnTouched:(id)sender{
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    if(self.contact){
        [self.contact setValue:firstNameFld.text forKey:@"firstname"];
        [self.contact setValue:addressFld.text forKey:@"address"];
        [self.contact setValue:lastNameFld.text forKey:@"lastname"];
        [self.contact setValue:phoneFld.text forKey:@"phone"];
        [self.contact setValue:emailFld.text forKey:@"email"];
    }else{
        NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contacts" inManagedObjectContext:context];
        [newContact setValue:firstNameFld.text forKey:@"firstname"];
        [newContact setValue:addressFld.text forKey:@"address"];
        [newContact setValue:lastNameFld.text forKey:@"lastname"];
        [newContact setValue:phoneFld.text forKey:@"phone"];
        [newContact setValue:emailFld.text forKey:@"email"];
    }
    
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)sendBtnTouched:(id)sender{
    
    MFMailComposeViewController *mailC = [[MFMailComposeViewController alloc]init];
    mailC.mailComposeDelegate = self;
    //[mailC setSubject:@""];
    NSString *message = [NSString stringWithFormat:@"Hello %@,\n\n", firstNameFld.text];
    [mailC setMessageBody:message isHTML:NO];
    [self presentViewController:mailC animated:YES completion:^{
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
