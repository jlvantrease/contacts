//
//  ViewController.m
//  Contacts
//
//  Created by Lyndy Tankersley on 2/23/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    contactsTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    contactsTableView.translatesAutoresizingMaskIntoConstraints = NO;
    contactsTableView.delegate = self;
    contactsTableView.dataSource = self;
    [self.view addSubview:contactsTableView];
    
    NSDictionary* viewsDictionary = NSDictionaryOfVariableBindings(contactsTableView);
    NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contactsTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contactsTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonTouched:)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [self loadData];
}

-(void)loadData{
    
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Contacts"];
    NSArray* array = [[context executeFetchRequest:fetchRequest error:nil]mutableCopy];
    arrayOfContacts = [array mutableCopy];
    [contactsTableView reloadData];
    
}

-(void)addButtonTouched:(id)sender{
    ContactsDetailController* cdc = [ContactsDetailController new];
    [self.navigationController pushViewController:cdc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayOfContacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* object = arrayOfContacts[indexPath.row];
    cell.textLabel.text = [object valueForKey:@"lastname"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:(NSIndexPath *) indexPath animated:YES];
    
    ContactsDetailController* cdc = [ContactsDetailController new];
    cdc.contact = arrayOfContacts[indexPath.row];
    [self.navigationController pushViewController:cdc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
