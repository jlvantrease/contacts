//
//  ViewController.h
//  Contacts
//
//  Created by Lyndy Tankersley on 2/23/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsDetailController.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView* contactsTableView;
    NSMutableArray* arrayOfContacts;
}


@end

