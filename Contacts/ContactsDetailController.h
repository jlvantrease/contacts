//
//  ContactsDetailController.h
//  Contacts
//
//  Created by Lyndy Tankersley on 2/23/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface ContactsDetailController : UIViewController
{
    UITextField* firstNameFld;
    UITextField* lastNameFld;
    UITextField* phoneFld;
    UITextField* addressFld;
    UITextField* emailFld;
}

@property (nonatomic, strong) NSManagedObject* contact;

@end
